**yarn**: install dependencies

**yarn start**: run project in dev mode

**yarn add <package_name>**: add new package/library to our project

**yarn remove <package_name>**: add new package/library to our project

**run project local** npm run start

**run project development** yarn run start:dev

**run project production** yarn run start:prod

**deploy project**
