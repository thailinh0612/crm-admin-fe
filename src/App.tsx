import React from "react";
import Routes from "@/pages/routes";
import withAuthPersist from "./stores/Authentication/withAuthPersist";
import "@/stores/middlewares/logger";
import "@/stores/middlewares/persistent";

const App: React.FC = () => {
  return <Routes />;
};

export default withAuthPersist(App);
