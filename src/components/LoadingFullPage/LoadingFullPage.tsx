import React from "react";
import { Spin } from "antd";

import "./LoadingFullPage.scss";

const LoadingFullPage: React.FC = () => {
  return (
    <div className="LoadingFullPage">
      <Spin size="large" />
    </div>
  );
};

export default LoadingFullPage;
