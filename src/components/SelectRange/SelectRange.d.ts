export interface SelectRangeProps {
  onClickSelectRange: CallableFunction;
  name: string;
}

export interface valueSelectRange {
  from: string;
  to: string;
}
