import { Form, InputNumber } from "antd";
import React from "react";

import { SelectRangeProps } from "./SelectRange.d";
import "./SelectRange.scss";

const SelectRange: React.FC<SelectRangeProps> = ({
  onClickSelectRange,
  name,
}) => {
  const handleApply = () => {
    onClickSelectRange();
  };

  return (
    <div className="SelectRange">
      <Form.Item>
        <Form.Item name={[`${name}`, "from"]}>
          <InputNumber
            className="SelectRange-input"
            placeholder="From"
            name="from"
          />
        </Form.Item>
        <Form.Item name={[`${name}`, "to"]}>
          <InputNumber
            className="SelectRange-input"
            placeholder="To"
            name="to"
          />
        </Form.Item>
      </Form.Item>

      <button
        className="SelectRange-button"
        type="button"
        onClick={handleApply}
        aria-hidden="true"
      >
        Apply
      </button>
    </div>
  );
};

export default SelectRange;
