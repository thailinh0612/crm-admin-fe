/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from "react";
import { Pagination, Table } from "antd";
import { TableProps } from "@/components/Tables/Tables.d";
import "./Tables.scss";
import usePage from "@/stores/CommonPage/commonPage";
import get from "lodash/get";

const Tables: React.FC<TableProps> = ({
  columns,
  dataSource,
  total,
  loading,
  rowKey,
  components,
  rowClassName,
  scroll = { x: 1000 }
}) => {
  const [statePage, actionPage] = usePage();

  const handleChangePagination = (page: number, pageSize: any): void => {
    actionPage.setPagination(page, pageSize);
  };

  return (
    <div className="Tables">
      <Table
        size={"small"}
        scroll={scroll}
        sticky={true}
        rowKey={(record) => get(record, `${rowKey}`, "")}
        loading={loading}
        pagination={false}
        columns={columns}
        dataSource={dataSource}
        components={components}
        rowClassName={rowClassName}
        className={"table-striped-rows"}
      />
      <Pagination
        style={{ marginTop: 16 }}
        current={statePage.params.page}
        pageSize={statePage.params.per_page}
        total={total}
        onChange={handleChangePagination}
      />
    </div>
  );
};

export default Tables;
