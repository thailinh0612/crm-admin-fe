import { DataTable } from "@/types/people";

export interface TableProps {
  page?: string;
  columns?: any;
  dataSource?: any;
  total: number;
  className?: string;
  loading?: boolean;
  detailInfo?: TabDetailInfoType;
  rowKey?: string;
  components?: TableComponents<RecordType>;
  rowClassName?: any;
  scroll?: {
    x?: number,
    y?: number
  }
}

export interface ColumnType {
  title?: string;
  dataIndex: string;
  className?: string;
  key?: string;
  width?: number | string;
  fixed?: "left" | undefined;
  ellipsis?: boolean;
  status: boolean;
  required?: boolean;
  sorter?: (a: DataTable, b: DataTable) => number;
  render?: any;
}
