import React from "react";
import { UserOutlined } from "@ant-design/icons";

import { Paths } from "@/pages/routesString";

export const attributes: {
  icon: JSX.Element;
  title: React.ReactElement | string;
  text: string;
  bottomLine: string;
  path: string;
  key: string;
}[] = [
  {
    icon: <UserOutlined />,
    title: "Tổng đài CSKH",
    text: "Tổng đài CSKH",
    bottomLine: "none",
    path: Paths.CustomerSupport,
    key: "customer_support",
  },
];
