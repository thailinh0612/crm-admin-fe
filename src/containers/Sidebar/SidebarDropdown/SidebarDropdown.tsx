import React from "react";
import { Menu } from "antd";
import "./SidebarDropdown.scss";
import { SidebarDropdownProps } from "./SidebarDropdown.d";

const SidebarDropdown = ({
  attributes,
  onClickOption,
}: SidebarDropdownProps): JSX.Element => (
  <Menu className="SidebarDropdown">
    {attributes.map((item, index) => {
      const isLastItem = attributes.length === index + 1;
      return (
        <Menu.Item
          style={{ borderTop: isLastItem ? "1px solid black" : "" }}
          key={item.text}
          icon={item?.icon}
          onClick={() => onClickOption(item.text)}
        >
          {isLastItem ? item.text : <p>{item.text}</p>}
        </Menu.Item>
      );
    })}
  </Menu>
);

export default SidebarDropdown;
