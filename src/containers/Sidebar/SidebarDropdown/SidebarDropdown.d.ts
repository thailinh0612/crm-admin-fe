export interface SidebarDropdownProps {
  attributes: { text: string; icon?: JSX.Element }[];
  onClickOption: (optionName: string) => void;
}
