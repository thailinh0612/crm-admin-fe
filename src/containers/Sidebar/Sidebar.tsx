import React from "react";
import { Layout, Menu } from "antd";
import { generatePath } from "react-router-dom";
import { get } from "lodash";
// import useAuthentication from "@/stores/Authentication/authentication";
import { Paths } from "@/pages/routesString";
import { attributes } from "./Sidebar.data";
import "./Sidebar.scss";
import { SidebarProps } from "./Sidebar.d";
import { useLocation } from "react-router-dom";
import useAPI from "@/api/useAPI";
import configAPI from "@/config/index";
const { Sider } = Layout;
const { SubMenu } = Menu;

const Sidebar: React.FC<SidebarProps> = () => {
  const { pathname } = useLocation();
  let pbxsId = "";
  let defaultOpenKeys = "";
  if (pathname.startsWith("/pbxs")) {
    pbxsId = pathname.split("/")[2];
  }
  if (pathname.endsWith("extensions")) {
    defaultOpenKeys = `extensions_${pbxsId}`;
  } else if (pathname.endsWith("call-logs")) {
    defaultOpenKeys = `call_logs_${pbxsId}`;
  } else if (pathname.endsWith("hotlines")) {
    defaultOpenKeys = `hotlines_${pbxsId}`;
  }

  const { data } = useAPI({
    url: configAPI.API.PBXS_SERVICE,
    loadInitialState: true,
    method: "GET",
  });

  const handleCallLogs = (callLog: any) => {
    window.location.href = generatePath(Paths.CallLogs, {
      id: get(callLog, "id"),
    });
  };

  const handleExtensions = (callLog: any) => {
    window.location.href = generatePath(Paths.Extensions, {
      id: get(callLog, "id"),
    });
  };
  const handleHotlines = (callLog: any) => {
    window.location.href = generatePath(Paths.Hotlines, {
      id: get(callLog, "id"),
    });
  };

  const renderMenuTitle = (title: string) => {
    return <div style={{ fontWeight: 600, fontSize: 16 }}>{title}</div>;
  };

  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      // collapsible
      zeroWidthTriggerStyle={{ top: 4 }}
      className="sider-container"
    >
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={[defaultOpenKeys]}
        defaultOpenKeys={[get(attributes, "[0].key", ""), pbxsId]}
      >
        <Menu.Item>
          <div className="logo">
            <span>RELIC.VN</span>
          </div>
        </Menu.Item>
        {attributes?.map((att, attIndex) => {
          return (
            <SubMenu key={`${att.key}`} icon={att.icon} title={att.text}>
              {get(data, "data", []).map((i: any, index: number) => {
                return (
                  <SubMenu
                    key={get(i, "id", "")}
                    title={renderMenuTitle(i.name)}
                  >
                    <Menu.Item
                      key={`call_logs_${get(i, "id", "")}`}
                      onClick={() => handleCallLogs(i)}
                    >
                      Lịch sử cuộc gọi
                    </Menu.Item>
                    <Menu.Item
                      key={`extensions_${get(i, "id", "")}`}
                      onClick={() => handleExtensions(i)}
                    >
                      Quản lí máy lẻ
                    </Menu.Item>
                    <Menu.Item
                      key={`hotlines_${get(i, "id", "")}`}
                      onClick={() => handleHotlines(i)}
                    >
                      Đầu số Hotlines
                    </Menu.Item>
                  </SubMenu>
                );
              })}
            </SubMenu>
          );
        })}
      </Menu>
    </Sider>
  );
};

export default Sidebar;
