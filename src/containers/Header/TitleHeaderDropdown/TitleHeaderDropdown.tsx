import React from "react";
import { Dropdown, Menu } from "antd";
import { DownOutlined } from "@ant-design/icons";

import "./TitleHeaderDropdown.scss";

const TitleHeaderDropdown: React.FC = () => {
  return (
    <Dropdown
      overlay={
        <Menu className="ListElement">
          <Menu.Item className="ListElement-element">
            Company Dashboard
          </Menu.Item>
          <Menu.Item className="ListElement-element">Phong Gia</Menu.Item>
        </Menu>
      }
      trigger={["click"]}
      className="TitleHeaderDropdown"
    >
      <div className="TitleHeaderDropdown-container">
        Phong Gia <DownOutlined />
      </div>
    </Dropdown>
  );
};

export default TitleHeaderDropdown;
