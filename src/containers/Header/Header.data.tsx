import React from "react";
import { LogoutOutlined } from "@ant-design/icons";

export const userOptions = [
  {
    key: "logout",
    title: "Đăng xuất",
    icon: <LogoutOutlined />,
  },
];
