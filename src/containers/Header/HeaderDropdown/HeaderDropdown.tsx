import React from "react";
import { Menu } from "antd";

import { attributes } from "@/containers/Header/HeaderDropdown/HeaderDropdown.data";
import "./HeaderDropdown.scss";

const HeaderDropdown = (): JSX.Element => (
  <Menu className="HeaderDropdown">
    {attributes.map((item) => (
      <Menu.Item className="HeaderDropdown-element" key={item.text}>
        {item.icon}
        <p className="HeaderDropdown-element-text">{item.text}</p>
      </Menu.Item>
    ))}
  </Menu>
);

export default HeaderDropdown;
