import React from "react";
import {
  BankOutlined,
  CheckSquareFilled,
  DollarOutlined,
  ShoppingFilled,
  UserOutlined,
} from "@ant-design/icons";

export const attributes: { icon: JSX.Element; text: string }[] = [
  { icon: <UserOutlined />, text: "New person" },
  { icon: <BankOutlined />, text: "New company" },
  { icon: <DollarOutlined />, text: "New Opportunity" },
  { icon: <CheckSquareFilled />, text: "New Task" },
  { icon: <ShoppingFilled />, text: "New Project" },
];
