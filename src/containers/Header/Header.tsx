import React, { useState } from "react";
import { Layout, Avatar, Row, Col, Popover } from "antd";
import { HeaderProps } from "@/containers/Header/Header.d";
import { LogoutOutlined } from "@ant-design/icons";
import "./Header.scss";
import useAuthentication from "@/stores/Authentication/authentication";

const Header: React.FC<HeaderProps> = () => {
  const [showedOptions, setShowOptions] = useState<boolean>(false);
  const [state, authenAction] = useAuthentication();
  const userName = state?.user.userName;
  const avatar = userName?.charAt(0).toUpperCase();

  const toggleUserOptions = () => {
    setShowOptions(!showedOptions);
  };

  const onClickOption = (optionName: string) => {
    switch (optionName) {
      default:
        return authenAction.logout();
    }
  };

  const renderOptions = () => {
    return (
      <div className="user-options-container">
        <div className="user-option" onClick={() => onClickOption("")}>
          <span>Logout </span>
          <LogoutOutlined />
        </div>
      </div>
    );
  };

  const handleUserOptionsChange = (showedOptions: boolean) => {
    setShowOptions(showedOptions);
  };

  return (
    <Layout.Header
      style={{
        color: "white",
        height: 48,
        lineHeight: "48px",
        padding: "0 16px",
        position: "sticky",
        top: 0,
      }}
    >
      <Row justify={"space-between"}>
        <Col flex={0}></Col>
        <Col flex={0}>
          <Popover
            content={renderOptions}
            trigger="focus"
            visible={showedOptions}
            onVisibleChange={handleUserOptionsChange}
            placement="bottomRight"
          >
            <span style={{ marginRight: 16 }}>{userName}</span>
            <span onClick={toggleUserOptions}>
              <Avatar className="avatar-options">{avatar}</Avatar>
            </span>
          </Popover>
        </Col>
      </Row>
    </Layout.Header>
  );
};

export default Header;
