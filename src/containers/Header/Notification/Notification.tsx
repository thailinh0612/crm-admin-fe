import React, { useState } from "react";
import { Menu, Button, Drawer } from "antd";
import {
  BellFilled,
  ArrowRightOutlined,
  CheckOutlined,
  SettingFilled,
} from "@ant-design/icons";

import "./Notification.scss";

const Notification: React.FC = () => {
  const [visible, setVisibleDrawer] = useState<boolean>(false);

  const toggleDrawer = (): void => {
    setVisibleDrawer(!visible);
  };

  return (
    <div className="Notifications">
      <Button
        type="link"
        className="Header-add-dropdown"
        onClick={toggleDrawer}
      >
        <BellFilled style={{ fontSize: "18px" }} />
      </Button>
      <Drawer
        className={`Header-add-notification ${
          visible === false ? "active" : ""
        }`}
        placement="right"
        closable={false}
        onClose={toggleDrawer}
        visible={visible}
        mask={false}
      >
        <div className="Notifications">
          <Menu className="Notification-container">
            <div className="Notifications-header">
              <Button
                type="link"
                className="Notifications-header-button"
                onClick={toggleDrawer}
              >
                <ArrowRightOutlined />
              </Button>

              <div className="Notifications-header-text">
                <p>Notifications</p>
              </div>

              <CheckOutlined />

              <SettingFilled />
            </div>

            <div className="Notifications-content">
              <div> &nbsp;</div>
            </div>
          </Menu>
        </div>
      </Drawer>
    </div>
  );
};

export default Notification;
