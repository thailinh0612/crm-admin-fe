import React from "react";
import { Layout } from "antd";
import { DashboardLayoutProps } from "@/layouts/DashboardLayout/DashboardLayout.d";
import Sidebar from "@/containers/Sidebar/Sidebar";
import Header from "@/containers/Header/Header";

const DashboardLayout: React.FC<DashboardLayoutProps> = ({ children }) => {
  return (
    <Layout>
      <Sidebar />
      <Layout
        className="site-layout"
        style={{
          overflow: "auto",
          height: "100vh",
          transition: "all 0.2s",
        }}
      >
        <Header />
        <Layout.Content
          style={{ minHeight: "cal(100vh-48px)", overflow: "auto" }}
        >
          {children}
        </Layout.Content>
      </Layout>
    </Layout>
  );
};

export default DashboardLayout;
