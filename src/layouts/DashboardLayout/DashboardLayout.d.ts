import { JsxAttribute } from "typescript";

export interface DashboardLayoutProps {
  children?: React.ReactNode;
}
