import React from "react";
import "./AnonymousUserLayout.scss";

const AnonymousUserLayout: React.FC = ({ children }) => {
  return <div className="full-screen anonymous-layout">{children}</div>;
};

export default AnonymousUserLayout;
