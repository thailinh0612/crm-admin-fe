export interface Params {
  [key: string]: any;
}

interface DataProperty {
  url: string;
  initialValue?: any;
  params?: AnyObject | string | undefined;
  method?: Method;
  headers?: AnyObject;
  payload?: object | string;
  loadInitialState?: boolean;
}

export type useApiFunction = (params: DataProperty) => any;
