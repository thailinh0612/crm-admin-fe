import { useState, useEffect } from "react";
import { get } from "lodash";
import axios from "axios";

import API from "./index";
import { Params, useApiFunction } from "./useAPI.d";
import { GET_METHOD } from "./api.constants";
import { useStoreAPI } from "./storeAPI";

/*
  custom hook for performing GET request
*/

const useAPI: useApiFunction = ({
  url,
  initialValue = [],
  params = {},
  method = GET_METHOD,
  payload = "",
  headers = {},
  loadInitialState = false,
}) => {
  const [data, setData] = useState(initialValue);
  const [currentInitialState, setCurrentInitialState] =
    useState(loadInitialState);
  const [currentUrl, setCurrentUrl] = useState(url);
  const [currentMethod, setCurrentMethod] = useState(method);
  const [currentParams, setCurrentParams] = useState<Params | undefined>(
    params
  );
  const [currentPayLoad, setCurrentPayLoad] = useState(payload);
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [state, actions] = useStoreAPI();

  const cancelTokenSource = axios.CancelToken.source();

  useEffect(() => {
    let isSubscribed = true;
    const fetchData = async () => {
      let newStore = {
        ...state,
        fetching: true,
        error: false,
        faults: "",
      };
      if (currentMethod === GET_METHOD) {
        newStore = {
          ...newStore,
          entityAction: "",
        };
      }
      try {
        actions.setStore(newStore);
        setLoading(true);
        const response = await API({
          url: currentUrl,
          params: currentParams,
          method: currentMethod,
          headers,
          data: currentPayLoad,
          cancelTokenSource,
        });

        if (isSubscribed) {
          setData(response);
          actions.setStore({
            ...state,
            fetching: false,
            error: false,
            data: response,
            faults: "",
          });
        }
      } catch (error) {
        const userMessage = get(
          error,
          "response.data.userMessage",
          "Unable to process the request"
        );

        // do not show toast error when cancel token
        if (
          ((Object.keys(error).length > 2 &&
            userMessage !== "Unable to process the request") ||
            Object.keys(error).length === 1) &&
          isSubscribed
        ) {
          // should create redux store manage api state (fetching, error, fault, .....)
          actions.setError(true);
          actions.setFaults(userMessage);
          actions.setFetching(false);
        }
      } finally {
        if (isSubscribed) {
          actions.setLoaded(true);
          setLoading(false);
        }
      }
    };
    if (currentInitialState) {
      fetchData();
    }
    return () => {
      actions.setStore({
        fetching: false,
      });
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      cancelTokenSource && cancelTokenSource.cancel();
      isSubscribed = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    actions,
    currentParams,
    currentUrl,
    currentMethod,
    currentPayLoad,
    currentInitialState,
    refresh,
  ]);

  return {
    loading,
    data,
    actions,
    state,
    currentMethod,
    refresh,
    setCurrentUrl,
    setCurrentParams,
    setCurrentInitialState,
    setCurrentMethod,
    setCurrentPayLoad,
    setRefresh,
  };
};

export default useAPI;
