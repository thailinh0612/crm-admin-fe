import axios, { AxiosError } from "axios";
import { defaultRegistry } from "react-sweet-state";
import ApiConfig from "@/config";
import API from "@/api/index";
import { API_ERROR_CODE } from "@/constants/enum";
import { Store } from "@/stores/Authentication/authentication";
import { AuthenticationStates } from "@/stores/Authentication/authenticationType";
import { Paths } from "@/pages/routesString";

export const handleRefreshToken = async (
  theRefreshToken: string,
  theAccessToken: string
) => {
  try {
    const response = await API({
      url: ApiConfig.API.USER_SERVICE_REFRESH_TOKEN,
      data: {
        accessToken: theAccessToken,
        refreshToken: theRefreshToken,
      },
      method: "POST",
    });
    return response;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log("ERR_REFRESH_TOKEN", error);
    throw error;
  }
};

axios.interceptors.request.use(
  async (config) => {
    if (config.url === ApiConfig.API.AUTHENTICATE_SERVICE) {
      return config;
    }

    // Check access token in redux store and add it to headers
    const authStore = await defaultRegistry.getStore(Store);
    const { storeState } = authStore;

    const authStates: AuthenticationStates = storeState.getState();
    const accessToken =
      Object.keys(authStates.authenticate).length > 0
        ? authStates.authenticate.accessToken
        : "";
    const newConfig = { ...config };
    if (newConfig.headers && !newConfig.headers[`Authorization`]) {
      newConfig.headers[`Authorization`] = `Bearer ${accessToken}`;
    }

    return newConfig;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (res) => res,
  async (error: AxiosError) => {
    if (`${error.response?.status}` === API_ERROR_CODE.UNAUTHORIZED) {
      window.location.href = Paths.Login;
    }

    return Promise.reject(error);
  }
);
