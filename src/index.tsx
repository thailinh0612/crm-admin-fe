import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { defaults } from "react-sweet-state";
import { QueryParamProvider } from "use-query-params";
import App from "@/App";
import "@/assets/styles/_global.scss";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";

console.log("===> ", process.env);

if (process.env.NODE_ENV === "development") {
  defaults.devtools = true;
}

if (process.env.REACT_APP_IS_SENTRY) {
  console.log("===> REACT_APP_IS_SENTRY: ", process.env.REACT_APP_IS_SENTRY);
  Sentry.init({
    dsn: "https://4824207be3cf4f27bee36b8008ffa6c6@o994708.ingest.sentry.io/5953357",
    integrations: [new Integrations.BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
}
export default ReactDOM.render(
  <Router>
    <QueryParamProvider ReactRouterRoute={Route}>
      <App />
    </QueryParamProvider>
  </Router>,
  document.getElementById("root")
);
