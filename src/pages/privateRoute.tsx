import React, { Suspense } from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import LoadingFullPage from "@/components/LoadingFullPage/LoadingFullPage";
import DashboardLayout from "@/layouts/DashboardLayout/DashboardLayout";
import useAuthentication from "@/stores/Authentication/authentication";

import { Paths } from "./routesString";

interface IProps {
  component: React.FC;
  path: string;
  exact?: boolean;
  pageName?: string;
  title?: string;
}

const renderRoute = (Component: React.FC) => (props: RouteProps) => {
  return (
    <Suspense fallback={<LoadingFullPage />}>
      <Component {...props} />
    </Suspense>
  );
};

const PrivateRoute: React.FC<IProps & RouteProps> = ({
  component,
  ...rest
}) => {
  const Layout = DashboardLayout;
  const [state] = useAuthentication();

  if (!state.loggedIn) {
    return (
      <Redirect
        to={{
          pathname: Paths.Login,
          state: { from: rest.location?.pathname },
        }}
      />
    );
  }

  if (state.loggedIn) {
    return (
      <Layout>
        <Route {...rest} render={renderRoute(component)} />
      </Layout>
    );
  }

  return (
    <Redirect to={{ pathname: Paths.AccessDenied, state: { from: "" } }} />
  );
};

export default PrivateRoute;
