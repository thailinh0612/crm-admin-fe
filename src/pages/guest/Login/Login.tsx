import React from "react";
import "./Login.scss";
import { useStoreAPI } from "@/api/storeAPI";
import API from "@/api/index";
import configUrlApi from "@/config/index";
import { get, isNull, isEmpty } from "lodash";
import { useHistory } from "react-router-dom";
import { Paths } from "@/pages/routesString";
import { LoginProps } from "./Login.d";
import useAuthentication from "@/stores/Authentication/authentication";
import { Card, Form, Input, Button } from "antd";
import { generatePath } from "react-router";
import { toast } from "react-toastify";

const Login: React.FC<LoginProps> = () => {
  const history = useHistory();
  const [, actionStoreAPI] = useStoreAPI();
  const [, actions] = useAuthentication();

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const onSubmit = async (data: any) => {
    try {
      actionStoreAPI.setFetching(true);
      const res = await API({
        url: configUrlApi.API.AUTHENTICATE_SERVICE,
        method: "POST",
        data,
      });

      if (!isNull(res) && !isEmpty(get(res, "data.token", null))) {
        const userInformation = {
          scope: get(res, "data.scope", ""),
          exp: get(res, "data.exp", null),
          userName: get(res, "data.username", ""),
        };

        const tokenData = {
          accessToken: get(res, "data.token", ""),
        };

        actions.login(tokenData, userInformation, true);
        const pbxsData = await API({
          url: configUrlApi.API.PBXS_SERVICE,
          method: "GET",
        });
        history.push(
          generatePath(Paths.CallLogs, {
            id: get(pbxsData, "data[0].id", ""),
          })
        );

        actionStoreAPI.setFetching(false);
      }
    } catch (error: any) {
      actionStoreAPI.setFetching(false);
      toast(error.response.data.message)
    }
  };

  return (
    <div className="login-container">
      <Card
        hoverable
        style={{ width: 450 }}
        title={<p className="login-title">Login</p>}
      >
        <Form
          name="basic"
          layout="vertical"
          initialValues={{
            remember: true,
          }}
          onFinish={onSubmit}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Account"
            name="account"
            rules={[
              {
                required: true,
                message: "Please input your account!",
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password size="large" />
          </Form.Item>
          <Form.Item>
            <Button
              shape="round"
              size="large"
              block
              type="primary"
              htmlType="submit"
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

export default Login;
