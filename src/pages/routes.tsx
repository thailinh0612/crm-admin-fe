import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import { Paths, Pages } from "@/pages/routesString";
import LoadingFullPage from "@/components/LoadingFullPage/LoadingFullPage";
import { TITLE_PAGE, PUBLIC_PAGES, DURATION, POSITION } from "@/constants/enum";
import withLayout from "@/layouts/withLayout";
import AnonymousUserLayout from "@/layouts/AnonymousUserLayout/AnonymousUserLayout";
import DashboardLayout from "@/layouts/DashboardLayout/DashboardLayout";
import useAuthentication from "@/stores/Authentication/authentication";

import PrivateRoute from "./privateRoute";

const Routes: React.FC = (): JSX.Element => {
  const [state] = useAuthentication();

  return (
    <div>
      <ToastContainer
        position={POSITION.TOP_CENTER}
        autoClose={DURATION.TOAST}
        hideProgressBar={false}
        closeButton
        closeOnClick
        pauseOnHover
        draggable={false}
        limit={1}
        style={{ height: 30 }}
      />
      <Switch>
        <Route
          path={"/"}
          exact
          component={withLayout(AnonymousUserLayout, () => (
            <Suspense fallback={<LoadingFullPage />}>
              <Pages.Login />
            </Suspense>
          ))}
        >
          <Redirect to={Paths.CallLogs} />
        </Route>
        <Route
          path={Paths.Login}
          exact
          component={withLayout(AnonymousUserLayout, () => (
            <Suspense fallback={<LoadingFullPage />}>
              <Pages.Login />
            </Suspense>
          ))}
        />
        <PrivateRoute
          title={TITLE_PAGE.CALL_LOG}
          path={Paths.CallLogs}
          // exact
          component={Pages.CallLog}
          pageName={PUBLIC_PAGES.NAME.CALLLOG}
        />
        <PrivateRoute
          title={TITLE_PAGE.EXTENSIONS}
          path={Paths.Extensions}
          // exact
          component={Pages.Extensions}
          pageName={PUBLIC_PAGES.NAME.CALLLOG}
        />
        <PrivateRoute
          title={TITLE_PAGE.EXTENSIONS}
          path={Paths.Hotlines}
          // exact
          component={Pages.Hotlines}
          pageName={PUBLIC_PAGES.NAME.CALLLOG}
        />
        <Route
          path={Paths.Rest}
          exact
          component={withLayout(
            state.loggedIn ? DashboardLayout : AnonymousUserLayout,
            () => (
              <Suspense fallback={<LoadingFullPage />}>
                <Pages.NotFound />
              </Suspense>
            )
          )}
        />
      </Switch>
    </div>
  );
};

export default Routes;
