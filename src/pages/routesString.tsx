import React from "react";

const Login = React.lazy(() => import("@/pages/guest/Login/Login"));
const CallLog = React.lazy(() => import("@/pages/auth/CallLog/CallLog"));
const Hotlines = React.lazy(() => import("@/pages/auth/Hotlines/Hotlines"));
const Extensions = React.lazy(
  () => import("@/pages/auth/Extensions/Extensions")
);
const NotFound = React.lazy(() => import("@/pages/guest/NotFound/NotFound"));

export const Paths = {
  Login: "/login",
  Auth: "/auth",
  Extensions: "/pbxs/:id/extensions",
  CallLogs: "/pbxs/:id/call-logs",
  Hotlines: "/pbxs/:id/hotlines",
  SipAccount: "/sip-account", // quan li may le
  CustomerSupport: "/customer-support",
  Apps: "/apps",
  Settings: "/settings",
  Rest: "*",
  AccessDenied: "/access-denied",
};

export const Pages = {
  Login,
  CallLog,
  NotFound,
  Extensions,
  Hotlines,
};

export const TitlePage = {};
