import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Button,
  Input,
  Modal,
  Space,
  Tooltip,
  Popconfirm,
  Form,
  Tag,
  Row,
  Col,
  Select,
  Card,
} from "antd";
import "./Extensions.scss";
import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import Tables from "@/components/Tables/Tables";
import useAPI from "@/api/useAPI";
import configAPI from "@/config/index";
import { ExtensionTableItem } from "@/pages/auth/Extensions/Extensions.d";
import usePage from "@/stores/CommonPage/commonPage";
import configUrlApi from "@/config/index";
import API from "@/api/index";
import isNull from "lodash/isNull";
import get from "lodash/get";
import isArray from "lodash/isArray";
import NewExtension from "./NewExtension/NewExtension";
import { toast } from "react-toastify";
import { getLabelFromValue } from "@/utils/helpers";
import {
  CallPermissionDropDownList,
  ConnectionTypeDropDownList,
} from "@/constants/droplist";
import FilterForm from "./FilterForm/FilterForm";

const ExtensionTools = ({
  editable,
  startEdit,
  cancelEdit,
  handleEdit,
  handleDelete,
}: any) => {
  return (
    <>
      {editable ? (
        <div style={{color: "#1890ff"}}>
          <span
            onClick={handleEdit}
            style={{
              marginRight: 8,
            }}
          >
            Lưu
          </span>
          <Popconfirm
            title="Thay đổi sẽ không được lưu, bạn có muốn huỷ?"
            okText="Có"
            cancelText="Không"
            onConfirm={cancelEdit}
          >
            <span>Huỷ</span>
          </Popconfirm>
        </div>
      ) : (
        <Space size="small">
          <Button size="small" onClick={startEdit}>
            <EditOutlined />
          </Button>
          <Popconfirm
            title="Bạn có muốn xoá?"
            okText="Có"
            cancelText="Không"
            onConfirm={handleDelete}
          >
            <Button size="small" danger>
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </Space>
      )}
    </>
  );
};

const EditableCell = ({
  editing,
  dataKey,
  title,
  inputType,
  record,
  index,
  children,
  control,
  required = true,
  ...restProps
}: any) => {
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataKey}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: required,
              message: `Nhập ${title}!`,
            },
          ]}
        >
          {control}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

const Extensions: React.FC<any> = () => {
  const [statePage, actionPage] = usePage();
  let extensionParams = useParams();
  const [form] = Form.useForm();
  const [addNewForm] = Form.useForm();
  const [editingKey, setEditingKey] = useState("");
  const [opendAddModal, setOpendAddModal] = useState(false);
  const [hotlines, setHotLines] = useState([]);
  const id = get(extensionParams, "id", "");

  const { data, loading, setCurrentParams, refresh, setRefresh } = useAPI({
    url: configAPI.API.EXTENSIONS_SERVICE.replace(":id", id),
    loadInitialState: true,
  });

  const reformatDataTable = get(data, "data", []).map((i: any) => {
    return {
      ...i,
      call_permission_label: getLabelFromValue(
        i?.call_permission,
        CallPermissionDropDownList
      ),
      connection_type_label: getLabelFromValue(
        i?.connection_type,
        ConnectionTypeDropDownList
      ),
    };
  });

  const handleDelete = (extensionId: string) => async () => {
    const res = await API({
      url: configUrlApi.API.DELETE_EXTENSION_SERVICE.replace(":id", id).replace(
        ":extensionId",
        extensionId
      ),
      method: "DELETE",
    });
    if (!isNull(res) && get(res, "status") === "success") {
      setRefresh(!refresh);
    }
  };

  const handleEdit = (number: string, extensionId: string) => async () => {
    try {
      const extensionEdited = await form.validateFields();
      const newTags = get(extensionEdited, ["tags"], []);

      await API({
        url: configAPI.API.EXTENSIONS_SERVICE.replace(":id", id).concat(
          `/${extensionId}`
        ),
        method: "PATCH",
        data: {
          ...extensionEdited,
          number,
          tags: isArray(newTags) ? newTags : newTags.split(","),
        },
      });

      setEditingKey("");
      setRefresh(!refresh);
    } catch (errInfo: any) {
      toast(errInfo?.response?.data.message);
    }
  };

  useEffect(() => {
    const id = get(extensionParams, "id", "");
    const getHotlines = async () => {
      const res = await API({
        url: configUrlApi.API.HOTLINES_SERVICE.replace(":id", id),
        method: "GET",
      });
      setHotLines(res?.data ?? []);
    };
    getHotlines();
  }, []);

  useEffect(() => {
    if (statePage.isPagination || statePage.isFilter || statePage.isSearch) {
      setCurrentParams(statePage.params);
    }
  }, [setCurrentParams, statePage]);

  const columns = [
    {
      title: "Số máy",
      dataIndex: "number",
      key: "number",
      fixed: "left",
      width: "100px",
    },
    {
      title: "Tài khoản",
      dataIndex: "account",
      key: "account",
    },
    {
      title: "Mật khẩu",
      dataIndex: "password",
      key: "password",
      editable: true,
      control: <Input />,
      render: (value: string) => (
        <Input.Password
          placeholder="input password"
          bordered={false}
          value={value}
        />
      ),
    },
    {
      title: "Quyền gọi",
      dataIndex: "call_permission_label",
      key: "call_permission",
      editable: true,
      control: (
        <Select>
          {CallPermissionDropDownList.map((item, index) => (
            <Select.Option key={index} value={`${item.value}`}>
              {item.label}
            </Select.Option>
          ))}
        </Select>
      ),
    },
    {
      title: "Mô tả",
      dataIndex: "note",
      key: "note",
      ellipsis: {
        showTitle: false,
      },
      render: (note: string) => {
        if (!note) {
          return "_";
        }

        return (
          <Tooltip placement="topLeft" title={note}>
            {note}
          </Tooltip>
        );
      },
      editable: true,
      control: <Input />,
      required: false,
    },
    {
      title: "Tags",
      dataIndex: "tags",
      key: "tags",
      editable: true,
      required: false,
      control: <Input />,
      render: (tags: string[] = [], index: number) => {
        if (tags.length === 0) {
          return "_";
        }
        return (
          <>
            {tags.map((tag) => (
              <Tag key={tag} color={"green"}>
                {tag || "-"}
              </Tag>
            ))}
          </>
        );
      },
    },
    {
      title: "Kiểu kết nối",
      dataIndex: "connection_type_label",
      key: "connection_type",
      editable: true,
      control: (
        <Select>
          {ConnectionTypeDropDownList.map((item, index) => (
            <Select.Option key={index} value={`${item.value}`}>
              {item.label}
            </Select.Option>
          ))}
        </Select>
      ),
    },
    {
      title: "Số hotline gọi ra",
      dataIndex: "outgoing_hotline_number",
      key: "outgoing_hotline_number",
      editable: true,
      control: (
        <Select>
          {hotlines.map((hotline: any) => (
            <Select.Option key={hotline.number} value={`${hotline.number}`}>
              {hotline?.number}
            </Select.Option>
          ))}
        </Select>
      ),
    },
    {
      title: "",
      key: "action",
      dataIndex: "id",
      fixed: "right",
      width: "84px",
      render: (text: string, record: ExtensionTableItem) => {
        const editable = isEditing(record);

        return (
          <ExtensionTools
            number={record.number}
            editable={editable}
            startEdit={edit(record)}
            cancelEdit={cancelEdit}
            handleEdit={handleEdit(record.number, record.id)}
            handleDelete={handleDelete(text)}
          />
        );
      },
    },
  ];

  const isEditing = (record: ExtensionTableItem) => record.id === editingKey;

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record: ExtensionTableItem) => {
        return {
          record,
          inputType: "text",
          dataKey: col.key,
          title: col.title,
          editing: isEditing(record),
          control: col.control,
          required: col.required,
        };
      },
    };
  });

  const edit = (record: ExtensionTableItem) => () => {
    form.setFieldsValue({
      ...record,
    });
    setEditingKey(record.id);
  };

  const cancelEdit = () => {
    setEditingKey("");
  };

  const onCloseAddNewModal = () => {
    setOpendAddModal(false);
    addNewForm.resetFields();
  };

  const handleFilter = (values: any) => {
    console.log("handleFilter", values);
    actionPage.setFilters({ ...values });
  };

  const onSubmit = () => {
    try {
      addNewForm
        .validateFields()
        .then(async (data) => {
          const id = get(extensionParams, "id", "");
          const payload: any = {
            pbx_id: id,
            number: data.number,
            password: data.password,
            connection_type: data.connection_type,
            call_permission: data.call_permission,
            outgoing_hotline_number: data.outgoing_hotline_number,
            note: data.note,
            tags: data.tags?.split(","),
          };

          const res = await API({
            url: configUrlApi.API.EXTENSIONS_SERVICE.replace(":id", id),
            method: "POST",
            data: payload,
          });
          if (res?.status === "success") {
            addNewForm.resetFields();
            onCloseAddNewModal();
            setRefresh(!refresh);
          }
        })
        .catch((info) => {
          console.log("Validate Failed:", info);
          toast(
            info.response?.data?.userMessage ??
              "Có lỗi xảy ra. Vui lòng thử lại sau"
          );
        });
    } catch (error) {}
  };

  return (
    <>
      <Modal
        title="Tạo mới"
        style={{ top: 80 }}
        visible={opendAddModal}
        onOk={onSubmit}
        onCancel={onCloseAddNewModal}
        width={900}
        okText="Lưu"
        cancelText="Huỷ"
      >
        <NewExtension form={addNewForm} />
      </Modal>

      <div className="page-title-container">
        <div className="title">Quản lý số liên lạc tổng đài</div>
      </div>
      <div style={{ padding: 16, width: "100%" }}>
        <Space direction="vertical" size={"middle"} style={{ width: "100%" }}>
          <Card hoverable>
            <Row style={{ marginBottom: 24 }}>
              <Col xs={{ span: 24, offset: 0 }} md={{ span: 8, offset: 0 }}>
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                  size={"middle"}
                  onClick={() => setOpendAddModal(true)}
                >
                  Thêm mới
                </Button>
              </Col>
            </Row>

            <FilterForm hotlines={hotlines} onSubmit={handleFilter} />
          </Card>

          <Card hoverable>
            <Form form={form} component={false}>
              <Tables
                components={{
                  body: {
                    cell: EditableCell,
                  },
                }}
                rowKey={"id"}
                columns={mergedColumns}
                loading={loading}
                dataSource={reformatDataTable}
                total={get(data, "total", [])}
              />
            </Form>
          </Card>
        </Space>
      </div>
    </>
  );
};

export default Extensions;
