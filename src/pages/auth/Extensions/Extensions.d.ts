export interface ExtensionTableItem {
  account: string;
  call_permission: string;
  connection_type: string;
  note: string;
  number: string;
  outgoing_hotline_number: string;
  password: string;
  pbx_id: string;
  tags: string[];
  id: string;
}
