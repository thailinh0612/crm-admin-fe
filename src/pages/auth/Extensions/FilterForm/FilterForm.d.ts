export interface FilterFormValues {
  state: string;
  fromTo: moment[];
}
interface FilterForm {
  search?: string;
  state?: string;
  from_time?: number;
  to_time?: number;
}

export interface FilterFormProps {
  onSubmit(values: FilterForm): void;
}

export const initialFilterValue: FilterForm = {
  search: undefined,
  state: undefined,
  from_time: undefined,
  to_time: undefined,
};
