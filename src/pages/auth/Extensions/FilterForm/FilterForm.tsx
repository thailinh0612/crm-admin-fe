import React from "react";
import { Form, Button, Row, Col, Select, Input } from "antd";
import { FilterFormValues } from "@/pages/auth/CallLog/FilterForm/FilterForm.d";
const { Option } = Select;

const filterControlSize = "middle";
const FilterForm: React.FC<any> = ({ onSubmit, hotlines = [] }) => {
  const [form] = Form.useForm();

  const onFinish = (values: FilterFormValues) => {
    onSubmit(values);
  };

  const handleReset = () => {
    form.resetFields();
    onSubmit({ outgoing_hotline_number: undefined, search: undefined });
  };

  return (
    <>
      <Form
        form={form}
        name="filter_call_logs"
        layout="vertical"
        onFinish={onFinish}
      >
        <Row gutter={12}>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 8, offset: 0 }}>
            <Form.Item label="Số máy" name="search">
              <Input placeholder="Nhập số máy" size={filterControlSize} />
            </Form.Item>
          </Col>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 8, offset: 0 }}>
            <Form.Item label="Số hotline gọi ra" name="outgoing_hotline_number">
              <Select
                allowClear={true}
                placeholder="Chọn số hotline gọi ra"
                size={filterControlSize}
              >
                {hotlines.map((item: any, index: number) => (
                  <Option value={`${item?.number}`} key={item?.id}>
                    {item?.number || "_"}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col
            xs={{ span: 24, offset: 0 }}
            md={{ span: 8, offset: 0 }}
            style={{
              display: "flex",
              justifyContent: "end",
              alignItems: "center",
            }}
          >
            <Button
              type="ghost"
              size={filterControlSize}
              htmlType="button"
              onClick={handleReset}
              style={{ marginRight: 16 }}
            >
              Đặt lại
            </Button>
            <Button type="primary" size={filterControlSize} htmlType="submit">
              Lọc báo cáo
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default FilterForm;
