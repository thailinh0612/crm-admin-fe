import React, { useEffect, useState } from "react";
import { Select, Form, Input, Row, Col } from "antd";
import { NewExtensionProps } from "@/pages/auth/Extensions/NewExtension/NewExtension.d";
import { ConnectionType, CallPermission } from "@/constants/enum";
import configUrlApi from "@/config/index";
import API from "@/api/index";
import { useParams } from "react-router-dom";
import get from "lodash/get";
import {
  CallPermissionDropDownList,
  ConnectionTypeDropDownList,
} from "@/constants/droplist";

const INPUT_SIZE = "middle";

const NewExtension: React.FC<NewExtensionProps> = ({ form }) => {
  const params = useParams();
  const [hotlines, setHotLines] = useState([]);

  useEffect(() => {
    const id = get(params, "id", "");
    const getHotlines = async () => {
      const res = await API({
        url: configUrlApi.API.HOTLINES_SERVICE.replace(":id", id),
        method: "GET",
      });
      setHotLines(res?.data ?? []);
    };
    getHotlines();
  }, []);

  return (
    <Form form={form} name="basic" layout="vertical">
      <Row gutter={[16, 16]}>
        <Col xs={{ span: 24 }} md={{ span: 8 }}>
          <Form.Item
            label="Số máy"
            name="number"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số máy!",
              },
            ]}
          >
            <Input size={INPUT_SIZE} autoComplete="false" />
          </Form.Item>
        </Col>
        <Col xs={{ span: 24 }} md={{ span: 8 }}>
          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input size={INPUT_SIZE} autoComplete="false" />
          </Form.Item>
        </Col>
        <Col xs={{ span: 24 }} md={{ span: 8 }}>
          <Form.Item
            label="Kiểu kết nối"
            name="connection_type"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập kiểu kết nối!",
              },
            ]}
          >
            <Select size={INPUT_SIZE}>
              {ConnectionTypeDropDownList.map((item, index) => (
                <Select.Option key={index} value={`${item.value}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col xs={{ span: 24 }} md={{ span: 8 }}>
          <Form.Item
            label="Quyền gọi"
            name="call_permission"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập quyền gọi!",
              },
            ]}
          >
            <Select size={INPUT_SIZE}>
              {CallPermissionDropDownList.map((item, index) => (
                <Select.Option key={index} value={`${item.value}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col xs={{ span: 24 }} md={{ span: 8 }}>
          <Form.Item
            label="Số hotline gọi ra"
            name="outgoing_hotline_number"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số hotline gọi ra!",
              },
            ]}
          >
            <Select size={INPUT_SIZE}>
              {hotlines.map((hotline: any) => (
                <Select.Option key={hotline.number} value={`${hotline.number}`}>
                  {hotline?.number}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col xs={{ span: 24 }} md={{ span: 8 }}>
          <Form.Item label="Tags" name="tags">
            <Input size={INPUT_SIZE} autoComplete="false" />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xs={{ span: 24 }} md={{ span: 16 }}>
          <Form.Item label="Mô tả" name="note">
            <Input.TextArea
              showCount
              maxLength={120}
              size={INPUT_SIZE}
              autoComplete="false"
            />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default NewExtension;
