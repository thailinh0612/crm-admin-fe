import { ConnectionType, CallPermission } from "@/constants/types";

export interface NewExtensionProps {
  form: Form;
}

export interface IExtensionFormValue {
  pbx_id: string;
  number: string;
  password: string;
  connection_type: ConnectionType;
  call_permission: CallPermission;
  outgoing_hotline_number: string;
  note: string;
  tags: string;
}

export interface ExtensionPayload {
  pbx_id: string;
  number: string;
  password: string;
  connection_type: ConnectionType;
  call_permission: CallPermission;
  outgoing_hotline_number: string;
  note: string;
  tags: string[];
}
