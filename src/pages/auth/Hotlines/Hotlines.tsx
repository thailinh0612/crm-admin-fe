import Tables from "@/components/Tables/Tables";
import useAPI from "@/api/useAPI";
import { Space, Card, Tag } from "antd";
import usePage from "@/stores/CommonPage/commonPage";
import get from "lodash/get";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import configAPI from "@/config/index";

const columns = [
    {
        title: "ID",
        dataIndex: "id",
        key: "id",
    },
    {
        title: "Number",
        dataIndex: "number",
        key: "number",
    },
    {
        title: "Mô tả",
        dataIndex: "note",
        key: "note",
    },
    {
        title: "Tags",
        dataIndex: "tags",
        key: "tags",
        editable: true,
        required: false,
        render: (tags: string[] = [], index: number) => {
            return (
                <>
                    {tags.map((tag) => (
                        <Tag key={tag} color={"green"}>
                            {tag || "-"}
                        </Tag>
                    ))}
                </>
            );
        },
    },
]

const Hotlines: React.FC<any> = ({ form }) => {
    const [statePage, actionPage] = usePage();
    let callLogsParams = useParams();
    const { data, loading, setCurrentParams } = useAPI({
        url: configAPI.API.HOTLINES_SERVICE.replace(
            ":id",
            get(callLogsParams, "id", "")
        ),
        loadInitialState: true,
    });
    console.log({ data })
    useEffect(() => {
        if (statePage.isPagination || statePage.isFilter || statePage.isSearch) {
            setCurrentParams(statePage.params);
        }
    }, [setCurrentParams, statePage]);

    return (
        <>
            <div className="page-title-container">
                <div className="title">Hotline</div>
            </div>
            <div style={{ padding: 16, width: "100%" }}>
                <Card hoverable>
                    <Tables
                        rowKey={"id"}
                        columns={columns}
                        loading={loading}
                        dataSource={get(data, "data", [])}
                        total={get(data, "total", [])}
                    />
                </Card>
            </div>
        </>
    )
}

export default Hotlines