import React from "react";
import FileSaver from "file-saver";
import XLSX from "xlsx";
import { Button } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import configAPI from "@/config/index";
import axios from "axios";
import { useParams } from "react-router-dom";
import get from "lodash/get";
import omit from "lodash/omit";
import moment from "moment";
import { getLabelFromValue } from "@/utils/helpers";
import {
  CallDirectionDropDownList,
  CallStateDropDownList,
} from "@/constants/droplist";

const renameFile = (fileName: string) =>
  `${fileName}_${moment(new Date()).format("DD/MM/YYYY")}`;

const reformatData = (data: any = []) => {
  return data.map((item: any) => {
    return {
      id: item.id,
      direct:
        getLabelFromValue(item.direct, CallDirectionDropDownList) || "N/A",
      caller: item.caller || "N/A",
      callee: item.callee || "N/A",
      incoming_hotline_number: item.incoming_hotline_number || "N/A",
      outgoing_hotline_number: item.outgoing_hotline_number || "N/A",
      state: getLabelFromValue(item.state, CallStateDropDownList),
      answer_time: item.answer_time
        ? moment.unix(item.answer_time).format("DD/MM/YYYY HH:mm:ss")
        : "N/A",
      end_time: item.end_time
        ? moment.unix(item.end_time).format("DD/MM/YYYY HH:mm:ss")
        : "N/A",
      call_duration: item.call_duration,
    };
  });
};

const CSVSaver = ({ fileName, params }: any) => {
  const callLogsParams = useParams();
  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  const fileExtension = ".xlsx";

  const Heading = [
    {
      id: "ID cuộc gọi",
      direct: "Hướng gọi",
      caller: "Số gọi đến",
      callee: "Số máy nghe",
      incoming_hotline_number: "Số hotline gọi đến",
      outgoing_hotline_number: "Số hotline gọi ra",
      state: "Trạng thái cuộc gọi",
      answer_time: "Thời gian nhấc máy",
      end_time: "Thời gian kết thúc",
      call_duration: "Thời gian cuộc gọi",
    },
  ];

  const wscols = [
    {
      wch: Heading[0].id.length * 3,
    },
    { wch: Heading[0].direct.length * 1.5 },
    { wch: Heading[0].caller.length },
    { wch: Heading[0].callee.length },
    { wch: Heading[0].incoming_hotline_number.length },
    { wch: Heading[0].outgoing_hotline_number.length },
    { wch: Heading[0].state.length },
    { wch: Heading[0].answer_time.length * 1.5 },
    { wch: Heading[0].end_time.length * 1.5 },
    { wch: Heading[0].call_duration.length },
  ];

  const exportToCSV = async () => {
    try {
      const newFileName = `${renameFile(fileName)}_${fileExtension}`;
      const response = await axios({
        params: omit(params, ["page", "per_page"]),
        method: "GET",
        url: configAPI.API.CALL_LOGS_SERVICE.replace(
          ":id",
          get(callLogsParams, "id", "")
        ),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        paramsSerializer: (params) => {
          let result = "";
          Object.keys(params).forEach((key) => {
            if (params[key]) {
              result += `${key}=${encodeURIComponent(params[key])}&`;
            }
          });
          
          return result.substr(0, result.length - 1);
        },
      });

      const ws = XLSX.utils.json_to_sheet(Heading, {
        header: [
          "id",
          "direct",
          "caller",
          "callee",
          "incoming_hotline_number",
          "outgoing_hotline_number",
          "state",
          "answer_time",
          "end_time",
          "call_duration",
        ],
        skipHeader: true,
      });

      ws["!cols"] = wscols;
      XLSX.utils.sheet_add_json(
        ws,
        reformatData(get(response, ["data", "data"], [])),
        {
          header: [
            "id",
            "direct",
            "caller",
            "callee",
            "incoming_hotline_number",
            "outgoing_hotline_number",
            "state",
            "answer_time",
            "end_time",
            "call_duration",
          ],
          skipHeader: true,
          origin: -1, //ok
        }
      );
      const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
      const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
      const data = new Blob([excelBuffer], { type: fileType });
      FileSaver.saveAs(data, newFileName);
    } catch (e) {
      console.log({ e });
    }
  };

  return (
    <Button
      type="primary"
      icon={<DownloadOutlined />}
      size={"middle"}
      onClick={exportToCSV}
    >
      Xuất báo cáo ra file excel
    </Button>
  );
};

export default CSVSaver;
