export interface FilterFormValues {
  state: string;
  fromTo: moment[];
  incoming_hotline_number?: string;
  outgoing_hotline_number?: string;
}
interface FilterForm {
  search?: string;
  state?: string;
  from_time?: number;
  to_time?: number;
  incoming_hotline_number?: string;
  outgoing_hotline_number?: string;
}

export interface FilterFormProps {
  onSubmit(values: FilterForm): void;
  hotlines: any[]
}

export const initialFilterValue: FilterForm = {
  search: undefined,
  state: undefined,
  from_time: undefined,
  to_time: undefined,
  incoming_hotline_number: undefined,
  outgoing_hotline_number: undefined
};
