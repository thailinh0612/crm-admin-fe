import React, { useState } from "react";
import { Form, Input, DatePicker, Button, Row, Col, Select, Tag } from "antd";
import {
  FilterFormProps,
  FilterFormValues,
  initialFilterValue,
} from "@/pages/auth/CallLog/FilterForm/FilterForm.d";
import join from "lodash/join";
import { getStateColor } from "../CallLog";
import { CallStateDropDownList } from "@/constants/droplist";

const { RangePicker } = DatePicker;
const { Option } = Select;

const dateFormat = "DD/MM/YYYY";
const filterControlSize = "middle";
const FilterForm: React.FC<FilterFormProps> = ({ onSubmit, hotlines = [] }) => {
  const [form] = Form.useForm();
  const [searchValues, setSearchValues] = useState<string[]>([]);
  const [inputValue, setInputValue] = useState("");

  const handleClose = (removedValue: string) => {
    const newSearchValues = searchValues.filter(
      (value) => value !== removedValue
    );
    setSearchValues(newSearchValues);
  };

  const forMap = (tag: any) => {
    const tagElem = (
      <Tag
        style={{ marginTop: 8 }}
        closable
        onClose={(e) => {
          e.preventDefault();
          handleClose(tag);
        }}
        color="#87d068"
      >
        {tag}
      </Tag>
    );
    return (
      <span key={tag} style={{ display: "inline-block" }}>
        {tagElem}
      </span>
    );
  };

  const tagChild = searchValues.map(forMap);
  const handleInputChange = (e: any) => {
    setInputValue(e.target.value);
  };
  const handleInputConfirm = () => {
    let currentSearchValues: string[] = searchValues;
    let newSearchValues = inputValue.split(",");

    newSearchValues.map((value) => {
      if (currentSearchValues.indexOf(value) !== -1 || value === "") {
        return;
      }

      return currentSearchValues.push(value);
    });
    setSearchValues(currentSearchValues);
    setInputValue("");
  };

  const onFinish = (values: FilterFormValues) => {

    const filterObj = {
      search: searchValues.length !== 0 ? join(searchValues, " ") : undefined,
      state: values.state,
      from_time: values.fromTo ? values.fromTo[0].unix() : undefined,
      to_time: values.fromTo ? values.fromTo[1].unix() : undefined,
      incoming_hotline_number: values?.incoming_hotline_number,
      outgoing_hotline_number: values?.outgoing_hotline_number,
    };
    onSubmit(filterObj);
  };

  const handleReset = () => {
    form.resetFields();
    setSearchValues([]);
    onSubmit(initialFilterValue);
  };

  return (
    <>
      <Row style={{ marginBottom: 8 }}>
        <Col span={24}>{tagChild}</Col>
      </Row>
      <Form
        form={form}
        name="filter_call_logs"
        layout="vertical"
        onFinish={onFinish}
      >
        <Row gutter={{ xs: 0, sm: 0, md: 16 }}>
          <Col xs={{ span: 24 }} md={{ span: 8 }}>
            <Form.Item label="Danh sách số máy">
              <Input
                placeholder="Cách nhau bằng dấu phẩy"
                size={filterControlSize}
                type="text"
                value={inputValue}
                onChange={handleInputChange}
                onBlur={handleInputConfirm}
              />
            </Form.Item>
          </Col>
          <Col xs={{ span: 24 }} md={{ span: 8 }}>
            <Form.Item label="Trạng thái cuộc gọi" name="state">
              <Select
                allowClear={true}
                placeholder="Chọn trạng thái cuộc gọi"
                size={filterControlSize}
              >
                {CallStateDropDownList.map((item, index) => (
                  <Option value={`${item.value}`} key={index}>
                    <Tag color={`${getStateColor(item.value)}`}>
                      {item.label}
                    </Tag>
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={{ span: 24 }} md={{ span: 8 }}>
            <Form.Item label="Khoảng thời gian" name="fromTo">
              <RangePicker
                style={{ width: "100%" }}
                size={filterControlSize}
                format={dateFormat}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xs: 0, sm: 0, md: 16 }}>
          <Col xs={{ span: 24 }} md={{ span: 8 }}>
            <Form.Item
              label="Số hotline gọi đến"
              name="incoming_hotline_number"
            >
              <Select
                allowClear={true}
                placeholder="Số hotline gọi đến"
                size={filterControlSize}
              >
                {hotlines.map((item: any, index: number) => (
                  <Option value={`${item?.number}`} key={item?.id}>
                    {item?.number || "_"}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={{ span: 24 }} md={{ span: 8 }}>
            <Form.Item label="Số hotline gọi ra" name="outgoing_hotline_number">
              <Select
                allowClear={true}
                placeholder="Số hotline gọi ra"
                size={filterControlSize}
              >
                {hotlines.map((item: any, index: number) => (
                  <Option value={`${item?.number}`} key={item?.id}>
                    {item?.number || "_"}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={12} justify={"end"}>
          <Col>
            <Button
              type="ghost"
              size={filterControlSize}
              htmlType="button"
              onClick={handleReset}
            >
              Đặt lại
            </Button>
          </Col>
          <Col style={{ marginRight: 16 }}>
            <Button type="primary" size={filterControlSize} htmlType="submit">
              Lọc báo cáo
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default FilterForm;
