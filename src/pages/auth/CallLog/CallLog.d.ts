export interface CallLogProps {}
export interface TypeCallLog {
  key: number;
  avatar: string;
  fullName: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  prefix: string;
  suffix: string;
  email: string;
  ownedBy: string;
  phone: string;
  ownedBy: string;
  modified: string;
  created_by: string;
  street: string;
  city: string;
  country: string;
  state: string;
  zip_code: string;
  work_website: string;
  social: string;
  visibility: string;
  description: string;
  tag: string[];
  title: string;
  company: string;
  contact_type: string;
  lastTimeContacted: string;
  interaction: string;
  inactiveDays: number;
  follow: boolean;
}

export interface CallLogData {
  id: number;
  action_description: string;
  action_service_visit_type: string;
  address: string;
  appointment_date: string;
  appointment_time: string;
  assign_datetime: string;
  close_datetime: string;
  column2: string;
  contract_id: string;
  createdAt: string;
  customer_id: string;
  customer_name: string;
  customer_number: string;
  date_of_request: string;
  is_visited: string;
  last_modified_sro_id: string;
  original_sro_id: string;
  revisit: string;
  service_priority_priority_id: string;
  service_type_service_id: string;
  service_type_service_id_1: string;
  service_type_service_status: string;
  so_mch_model: string;
  so_number: string;
  so_serial_number: string;
  so_tech_code_2: string;
  symptom_symptom_id: string;
  terr_code: string;
  time_of_request: string;
  updatedAt: string;
  userId: string;
}

export interface CallLogRowItem {
  id: string;
  answer_time: string;
  end_time: string;
  start_time: string;
  audio_url: string;
  call_duration: number;
  callee: string;
  caller: string;
  conversation_duration: number;
  custom_data: any;
  events: string[];
  note: string;
  pbx_id: string;
  server_ip: string;
  state: string;
  tags: string[];
}
