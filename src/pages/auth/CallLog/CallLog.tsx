import React, { useEffect, useState } from "react";
import { Space, Tag, Card, Button } from "antd";
import get from "lodash/get";
import { CallLogProps } from "@/pages/auth/CallLog/CallLog.d";
import useAPI from "@/api/useAPI";
import "./CallLog.scss";
import usePage from "@/stores/CommonPage/commonPage";
import configAPI from "@/config/index";
import FilterForm from "./FilterForm/FilterForm";
import { useParams } from "react-router-dom";
import Tables from "@/components/Tables/Tables";
import { CallStatus } from "@/constants/enum";
import AudioControl from "@/pages/auth/CallLog/AudioControl/AudioControl";
import moment from "moment";
import { getLabelFromValue } from "@/utils/helpers";
import {
  CallDirectionDropDownList,
  CallStateDropDownList,
} from "@/constants/droplist";
import API from "@/api/index";
import CSVSaver from "./CSVSaver/CSVSaver";

export const getStateColor = (status: string) => {
  let color = "blue";
  switch (status) {
    case CallStatus.success:
      color = "green";
      break;
    case CallStatus.blocked:
      color = "gold";
      break;
    case CallStatus.miss:
      color = "red";
      break;
  }

  return color;
};

const CallLog: React.FC<CallLogProps> = () => {
  const [statePage, actionPage] = usePage();
  const [hotlines, setHotLines] = useState([]);
  let callLogsParams = useParams();
  const { data, loading, setCurrentParams } = useAPI({
    url: configAPI.API.CALL_LOGS_SERVICE.replace(
      ":id",
      get(callLogsParams, "id", "")
    ),
    loadInitialState: true,
  });

  useEffect(() => {
    const id = get(callLogsParams, "id", "");
    const getHotlines = async () => {
      const res = await API({
        url: configAPI.API.HOTLINES_SERVICE.replace(":id", id),
        method: "GET",
      });
      setHotLines(res?.data ?? []);
    };
    getHotlines();
  }, []);

  useEffect(() => {
    if (statePage.isPagination || statePage.isFilter || statePage.isSearch) {
      setCurrentParams(statePage.params);
    }
  }, [setCurrentParams, statePage]);

  const handleFilter = (values: any) => {
    actionPage.setFilters(values);
  };

  const columns = [
    {
      title: "ID cuộc gọi",
      dataIndex: "id",
      key: "call_log_id",
      width: 160,
      fixed: "left",
      render: (text: string) => (
        <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
          {text || "-"}
        </div>
      ),
    },
    {
      title: "Hướng gọi",
      dataIndex: "direct",
      key: "direct",
      render: (value: string) => {
        return <>{getLabelFromValue(value, CallDirectionDropDownList)}</>;
      },
    },
    {
      title: "Số gọi đến",
      dataIndex: "caller",
      key: "caller",
    },
    {
      title: "Số máy nghe",
      dataIndex: "callee",
      key: "callee",
    },
    {
      title: "Số hotline gọi đến",
      dataIndex: "incoming_hotline_number",
      key: "incoming_hotline_number",
      render: (value: string) => {
        return value ? value : "N/A";
      },
    },
    {
      title: "Số hotline gọi ra",
      dataIndex: "outgoing_hotline_number",
      key: "outgoing_hotline_number",
      render: (value: string) => {
        return value ? value : "N/A";
      },
    },
    {
      title: "Trạng thái cuộc gọi",
      dataIndex: "state",
      key: "state",
      render: (value: string) => {
        return (
          <Tag color={`${getStateColor(value)}`}>
            {getLabelFromValue(value, CallStateDropDownList) || "-"}
          </Tag>
        );
      },
    },
    {
      title: "Thời gian nhấc máy",
      dataIndex: "answer_time",
      key: "answer_time",
      render: (value: number) => (
        <div>
          {value ? moment.unix(value).format("DD/MM/YYYY HH:mm:ss") : "N/A"}
        </div>
      ),
    },
    {
      title: "Thời gian kết thúc",
      dataIndex: "end_time",
      key: "end_time",
      render: (value: number) => (
        <div>
          {value ? moment.unix(value).format("DD/MM/YYYY HH:mm:ss") : "N/A"}
        </div>
      ),
    },
    {
      title: "Thời gian cuộc gọi",
      dataIndex: "call_duration",
      key: "call_duration",
    },
    {
      title: "Ghi âm",
      dataIndex: "audio_url",
      key: "audio_url",
      render: (_: string, record: any) => {
        if (!record?.audio_url) return "N/A";

        return (
          <AudioControl
            url={`${process.env.REACT_APP_ROOT_URL}/${record?.audio_url}`}
          />
        );
      },
    },
  ];

  return (
    <>
      <div className="page-title-container">
        <div className="title">Báo cáo tổng đài</div>
      </div>
      <div style={{ padding: 16, width: "100%" }}>
        <Space direction="vertical" size={"middle"} style={{ width: "100%" }}>
          <Card hoverable>
            <FilterForm hotlines={hotlines} onSubmit={handleFilter} />
          </Card>
          <Card hoverable>
            <div className="export-container">
              <CSVSaver fileName="callogs" params={statePage.params} />
            </div>
            <Tables
              rowKey={"id"}
              columns={columns}
              loading={loading}
              dataSource={get(data, "data", [])}
              total={get(data, "total", [])}
              scroll={{ x: 1300 }}
            />
          </Card>
        </Space>
      </div>
    </>
  );
};

export default CallLog;
