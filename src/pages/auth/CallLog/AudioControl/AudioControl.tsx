import React, { useState, useEffect } from "react";
import { Button } from "antd";
import { PlayCircleOutlined, PauseCircleOutlined } from "@ant-design/icons";

const useAudio = (url: string) => {
  const [audio] = useState(new Audio(url));
  const [playing, setPlaying] = useState(false);

  const toggle = () => setPlaying(!playing);

  useEffect(() => {
    playing ? audio.play() : audio.pause();
  }, [playing]);

  useEffect(() => {
    audio.addEventListener("ended", () => setPlaying(false));
    return () => {
      audio.removeEventListener("ended", () => setPlaying(false));
    };
  }, []);

  return [playing, toggle];
};

const AudioControl: React.FC<any> = ({ url }: any) => {
  const [playing, toggle] = useAudio(url);

  return (
    <Button
      onClick={toggle}
      type="primary"
      icon={playing ? <PauseCircleOutlined /> : <PlayCircleOutlined />}
    >
      {playing ? "Pause" : "Play"}
    </Button>
  );
};

export default AudioControl;
