import { createStore, createHook, StoreActionApi } from "react-sweet-state";

import { StorePageType } from "@/stores/CommonPage/commonPage.d";

export const PAGE_STORE = "StorePage";

type StorePage = StoreActionApi<StorePageType>;
type Action = typeof actions;

export const initialParams = {
  page: 1,
  per_page: 10,
  // sort_order: 1
};

export const initialState: StorePageType = {
  isFilter: false,
  isSearch: false,
  isPagination: true,
  rowClick: {},
  params: initialParams,
};

export const actions = {
  setFilters: (filters: { [key: string]: string }) => ({
    setState,
    getState,
  }: StorePage) => {
    const preState = getState();
    setState({
      ...preState,
      params: {
        ...preState.params,
        ...filters,
      },
    });
  },
  setSearch: (search: { [key: string]: string }) => ({
    setState,
    getState,
  }: StorePage) => {
    const preState = getState();
    setState({
      ...preState,
      params: {
        ...preState.params,
        ...search,
      },
    });
  },
  setPagination: (page: number, pageSize?: any) => ({
    setState,
    getState,
  }: StorePage) => {
    const preState = getState();

    setState({
      ...preState,
      params: {
        ...preState.params,
        page,
        per_page: pageSize,
      },
    });
  },

  setRowClick: (dataRowClick: any) => ({ setState, getState }: StorePage) => {
    const preState = getState();
    setState({
      ...preState,
      rowClick: dataRowClick,
    });
  },
};

export const store = createStore<StorePageType, Action>({
  initialState,
  actions,
  name: PAGE_STORE,
});

const usePage = createHook(store);

export default usePage;
