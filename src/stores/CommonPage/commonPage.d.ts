export interface paginationInfo {
  page: number;
  size: number;
}
export interface filterInfo {}
export interface searchInfo {}

export interface StorePageType {
  isFilter: boolean;
  isSearch: boolean;
  isPagination: boolean;
  rowClick: { [key: string]: string };
  params: { page: number; per_page: number; filter?: string; search?: string };
}
