import databases from "@/cache";
import {
  createStore,
  createHook,
  StoreActionApi,
  createContainer,
} from "react-sweet-state";
import {
  AuthenticationStates,
  AuthenticateInformation,
  UserInformation,
} from "./authenticationType.d";

export const AUTHENTICATION_STORE = "StoreAuthentication";
type StoreApi = StoreActionApi<AuthenticationStates>;
type Actions = typeof actions;

export const initialState: AuthenticationStates = {
  user: {
    scope: "",
    exp: null,
    userName: "",
  },
  loggedIn: false,
  initiated: false,
  authenticate: {
    accessToken: "",
  },
};

export const actions = {
  login:
    (
      tokenPayload: AuthenticateInformation,
      userPayload: UserInformation,
      loggedIn: boolean
    ) =>
    ({ setState }: StoreApi) => {
      setState({
        user: userPayload,
        loggedIn,
        authenticate: tokenPayload,
      });
    },
  logout:
    () =>
    async ({ setState }: StoreApi) => {
      setState({ ...initialState });
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      await databases.removeItem(storeKey);
    },
  setAuthenticate:
    (auth: AuthenticateInformation) =>
    ({ setState, getState }: StoreApi) => {
      const prevState = getState();
      setState({
        ...prevState,
        authenticate: auth,
      });
    },
};

export const Store = createStore<AuthenticationStates, Actions>({
  initialState,
  actions,
  name: AUTHENTICATION_STORE,
});

const useAuthentication = createHook(Store);

export const storeKey = `${Store.key.join("__")}@__global__`;

type StoreContainerProps = {
  initialState: AuthenticationStates;
};
export const AuthenticationContainer = createContainer<
  AuthenticationStates,
  Actions,
  StoreContainerProps
>(Store, {
  onInit:
    () =>
    ({ setState }: StoreApi, { initialState }) => {
      setState({ ...initialState });
    },
});

export default useAuthentication;
