interface UserInformation {
  scope: string;
  exp: number | null;
  userName: string;
}

interface AuthenticationStates {
  user: UserInformation;
  loggedIn: boolean;
  initiated: boolean;
  authenticate: AuthenticateInformation;
}

interface AuthenticateInformation {
  accessToken: string;
}

export { AuthenticationStates, UserInformation, AuthenticateInformation };
