import { defaults, StoreState } from "react-sweet-state";
import { isDevEnv } from "@/config";

const logger = (storeState: StoreState<any>) => (next: any) => (fn: any) => {
  // eslint-disable-next-line no-console
  console.groupCollapsed("Store", storeState.key);
  // keep comment code to check middleware with store
  // eslint-disable-next-line no-console
  console.log("prev state:", JSON.parse(JSON.stringify(storeState.getState())));
  // eslint-disable-next-line no-console
  console.log("payload:", fn);
  const result = next(fn);
  // eslint-disable-next-line no-console
  console.log("next state:", JSON.parse(JSON.stringify(storeState.getState())));
  // eslint-disable-next-line no-console
  console.groupEnd();
  return result;
};

if (isDevEnv()) {
  defaults.middlewares.add(logger);
}
