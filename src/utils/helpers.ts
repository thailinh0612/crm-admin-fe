/* eslint-disable no-useless-escape */
import { DropdownItem } from "@/constants/enum";
import moment from "moment";

export const humanReadableTime = (time: string) => {
  const formatedTime = moment(moment.utc(time)).local();
  const duration = moment.duration(moment().diff(formatedTime));
  const seconds = duration.asSeconds();
  const humanizeDuration = moment.duration(-seconds, "seconds").humanize(true);

  return humanizeDuration;
};

export const urlSafe = (text: string) =>
  text
    .replace(/[^a-zA-Z0-9- ]/g, "") // remove invalid characters
    .replace(/\s\s+/g, " ") // trim whitespace
    .replace(/ /g, "-") // replace space with -
    .toLowerCase();

export const camelCase = (str: string) => {
  return str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (word: string, index: number) => {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replace(/\s+/g, "");
};

export const searchString = (
  target: string | string[],
  searchValue: string
) => {
  const searchKey = searchValue.toLowerCase();
  const searchTarget =
    target instanceof Array
      ? target.map((str) => str.toLowerCase())
      : target.toLowerCase();
  const searchResult =
    searchTarget instanceof Array
      ? !!searchTarget.filter((str) => str.includes(searchKey)).length
      : searchTarget.includes(searchKey);
  return searchResult;
};

export const regex = {
  htmlTagsInsensitive: /(<([^>]+)>)/i,
  htmlTagsGlobal: /(<([^>]+)>)/g,
  htmlTagsExcept: (tagName: string) => new RegExp(`\<(?!${tagName}).*?\>`, "g"),
  lineBreak: /\\n/i,
  email:
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i,
  emailDomain: /^[a-zA-Z0-9]{1,61}\.[a-zA-Z]{2,}$/g,
  url: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i,
  digits: /^\d+$/i,
  phoneNumber:
    /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/i,
};

export const truncateString = (content: string, maxLength: number) => {
  const contentLength = content.length;
  return `${content.slice(
    0,
    contentLength > maxLength ? maxLength : contentLength
  )}${contentLength > maxLength ? "..." : ""}`;
};

export const truncateStringByWords = (content: string, maxWords: number) => {
  const contentSplited = content.split(" ");
  if (maxWords >= contentSplited.length) {
    return content;
  }
  const contentSplitedOptimized = contentSplited.filter(
    (word, index) => index < maxWords
  );
  const contentTruncated = contentSplitedOptimized.join(" ");
  return `${contentTruncated}...`;
};

export const beautifyLongText = (content: string, maxLength: number) => {
  return truncateString(
    content.replace(regex.htmlTagsGlobal, "").replace(regex.lineBreak, ""),
    maxLength
  );
};

export const lockPageScroll = () => {
  const body = document.getElementById("body");
  body?.classList.add("lock-scroll");
};

export const unlockPageScroll = () => {
  const body = document.getElementById("body");
  body?.classList.remove("lock-scroll");
};

export const getPageQueryParam = (param: string) => {
  const params = new URLSearchParams(window.location.search);
  return params.get(param);
};

export const getLabelFromValue = (value: string, items: DropdownItem[]) => {
  return items.find((i) => i.value === value)?.label || "";
};
