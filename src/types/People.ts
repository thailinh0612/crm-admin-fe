import { CheckboxValueType } from "antd/lib/checkbox/Group";
import { Moment } from "moment";

export interface PeopleInput {
  fName?: string;
  mName?: string;
  lName?: string;
  prefix?: string;
  suffix?: string;
  company?: string;
  title?: string;
  owner?: string;
  email?: string;
  phone?: string;
  website?: string;
  social?: string;
  street?: string;
  country?: string;
  city?: string;
  state?: string;
  zipCode?: string;
  description?: string;
  tag?: string;
}

export interface DataFilter {
  interaction: {
    from: string;
    to: string;
  };
  activityType: {
    match: string;
    option: string[];
  };
  inactiveDay: {
    from: string;
    to: string;
  };
  lastContacted: null | [Moment | null, Moment | null];
  dateAdded: null | [Moment | null, Moment | null];
  ownedBy: {
    option: string[];
  };
  follow: {
    option: CheckboxValueType[];
  };
  contactType: {
    option: CheckboxValueType[];
  };
  company: {
    option: string[];
  };
  opportunities: {
    option: string[];
  };
  tags: {
    match: string;
    option: string[];
  };
  city: {
    option: string[];
  };
  state: {
    option: string[];
  };
  country: {
    option: string[];
  };
  zip: {
    option: string[];
  };
}
