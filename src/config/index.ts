/* eslint-disable import/no-cycle */
import dev from "./development";
import prod from "./production";

export interface IConfig {
  API: {
    ROOT_ENDPOINT: string;
    USER_SERVICE_REFRESH_TOKEN: string;
    PEOPLE_SERVICE: string;
    COMPANY_SERVICE: string;
    AUTHENTICATE_SERVICE: string;
    PBXS_SERVICE: string;
    DATA_SERVICE: string;
    CALL_LOGS_SERVICE: string;
    EXTENSIONS_SERVICE: string;
    DELETE_EXTENSION_SERVICE: string;
    HOTLINES_SERVICE: string;
  };
}

let config: IConfig = { ...dev };
const env = process.env.REACT_APP_ENV;
switch (env) {
  case "dev":
    config = dev;
    break;
  case "prod":
    config = prod;
    break;
  default:
    break;
}

export const isDevEnv = () => {
  return process.env.REACT_APP_ENV === "dev";
};
export const SESSION_TIMEOUT = Number(process.env.REACT_APP_SESSION_TIMEOUT);
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  env,
  ...config,
};
