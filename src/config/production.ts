// eslint-disable-next-line import/no-cycle
import { IConfig } from "./index";

export const REACT_APP_API_HOST =
  process.env.REACT_APP_ROOT_URL || "http://localhost:3004";
export const SEVICE_API_HOST =
  process.env.SERVICE_API_HOST || "http://localhost:3000";

const CONFIG: IConfig = {
  API: {
    ROOT_ENDPOINT: `${REACT_APP_API_HOST}`,
    USER_SERVICE_REFRESH_TOKEN: `${REACT_APP_API_HOST}/oauth/token`,
    PEOPLE_SERVICE: `${REACT_APP_API_HOST}/v1/api/contacts`,
    COMPANY_SERVICE: `${REACT_APP_API_HOST}/v1/api/accounts`,
    AUTHENTICATE_SERVICE: `${SEVICE_API_HOST}/users/authenticate`,
    DATA_SERVICE: `${SEVICE_API_HOST}/datas/datas`,
  },
};
export default CONFIG;
