// eslint-disable-next-line import/no-cycle
import { IConfig } from "./index";

export const REACT_APP_API_HOST =
  process.env.REACT_APP_ROOT_URL || "http://localhost:3004";

const CONFIG: IConfig = {
  API: {
    ROOT_ENDPOINT: `${REACT_APP_API_HOST}`,
    USER_SERVICE_REFRESH_TOKEN: `${REACT_APP_API_HOST}/oauth/token`,
    PEOPLE_SERVICE: `${REACT_APP_API_HOST}/v1/api/contacts`,
    COMPANY_SERVICE: `${REACT_APP_API_HOST}/v1/api/accounts`,
    AUTHENTICATE_SERVICE: `${REACT_APP_API_HOST}/auth`,
    PBXS_SERVICE: `${REACT_APP_API_HOST}/pbxs`,
    DATA_SERVICE: `${REACT_APP_API_HOST}/datas/datas`,
    CALL_LOGS_SERVICE: `${REACT_APP_API_HOST}/pbxs/:id/call_logs`,
    EXTENSIONS_SERVICE: `${REACT_APP_API_HOST}/pbxs/:id/extensions`,
    DELETE_EXTENSION_SERVICE: `${REACT_APP_API_HOST}/pbxs/:id/extensions/:extensionId`,
    HOTLINES_SERVICE: `${REACT_APP_API_HOST}/pbxs/:id/hotlines`,
  },
};
export default CONFIG;
