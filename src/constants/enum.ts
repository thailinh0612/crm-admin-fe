import { ToastPosition } from "react-toastify";

export const CLIENT_ID = "618606";

export const PUBLIC_PAGES = {
  NAME: {
    CALLLOG: "data",
    DASHBOARD: "dashboard",
    COMPANIES: "companies",
    OPPORTUNITIES: "opportunities",
    PROJECT: "projects",
    LOGIN: "login",
    ACCESS_DENIED: "access-denied-403",
  },
};

export const TITLE_PAGE = {
  CALL_LOG: "Call Log",
  EXTENSIONS: "Extensions",
  ACCESS_DENIED: "Access Denied",
};

export const NAME_PAGE = {
  CALL_LOG: "Call Log",
};

export const TYPE_FILTER = {
  CHECKBOX: "checkbox",
  SELECT: "select",
};

export const API_ERROR_CODE = {
  UNAUTHORIZED: "401",
};

export type AlertTypeOptions =
  | "info"
  | "success"
  | "warning"
  | "error"
  | undefined;

interface TYPE_OPTIONS {
  [type: string]: AlertTypeOptions;
}

export const TOAST_TYPE: TYPE_OPTIONS = {
  SUCCESS: "success",
  ERROR: "error",
  INFO: "info",
  WARNING: "warning",
};

interface POSITION_OPTIONS {
  [pos: string]: ToastPosition;
}

export const POSITION: POSITION_OPTIONS = {
  TOP_LEFT: "top-left",
  TOP_RIGHT: "top-right",
  TOP_CENTER: "top-center",
  BOTTOM_LEFT: "bottom-left",
  BOTTOM_RIGHT: "bottom-right",
  BOTTOM_CENTER: "bottom-center",
};

export const DURATION = {
  TOAST: 5000,
  TOAST_TRANSITION: 1000,
  BULK_POLLING_DELAY: 15000,
  UPDATE_NOTIFICATIONS: 60000,
  UPDATE_PROGRESS_JOB: 5000,
};

export const API_ERROR_CODE_MESSAGE = {
  DUPLICATE_PHONE_CONTACT: "CONTACT.ADD.DUPLICATE_PHONE",
  DUPLICATE_EMAIL_CONTACT: "CONTACT.ADD.DUPLICATE_EMAIL",
};

export enum CallStatus {
  success = "success",
  miss = "miss",
  blocked = "blocked",
}

export enum ConnectionType {
  secure = "secure",
  normal = "normal",
}

export enum CallPermission {
  mobile = "mobile",
  telephone = "telephone",
  internal = "internal",
}

export enum CallDirection {
  in = "in",
  out = "out",
  internal = "internal",
}

export interface DropdownItem {
  value: string;
  label: string;
}
