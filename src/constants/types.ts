export type FnType = (param?: object) => void;

export declare type ConnectionType = "secure" | "normal";
export declare type CallPermission = "mobile" | "telephone" | "internal";
