import { CallDirection } from './enum';
import {
  ConnectionType,
  CallPermission,
  DropdownItem,
  CallStatus,
} from "@/constants/enum";

export const CallPermissionDropDownList: DropdownItem[] = [
  {
    value: CallPermission.internal,
    label: "Nội bộ",
  },
  {
    value: CallPermission.mobile,
    label: "Di động",
  },
  {
    value: CallPermission.telephone,
    label: "Máy bàn",
  },
];

export const ConnectionTypeDropDownList: DropdownItem[] = [
  {
    value: ConnectionType.normal,
    label: "Bình thường",
  },
  {
    value: ConnectionType.secure,
    label: "Bảo mật",
  },
];

export const CallStateDropDownList: DropdownItem[] = [
  {
    value: CallStatus.success,
    label: "Thành công",
  },
  {
    value: CallStatus.blocked,
    label: "Chặn",
  },
  {
    value: CallStatus.miss,
    label: "Gọi nhỡ",
  },
];

export const CallDirectionDropDownList: DropdownItem[] = [
  {
    value: CallDirection.internal,
    label: "Gọi nội bộ",
  },
  {
    value: CallDirection.in,
    label: "Gọi vào",
  },
  {
    value: CallDirection.out,
    label: "Gọi ra",
  },
];
