const express = require("express");
const path = require("path");
require("dotenv").config();

const app = express();

const root = path.join(__dirname, "build");

app.use(express.static(root));
app.get("*", (req, res) => {
  res.sendFile("index.html", { root });
});

const port = process.env.PORT || "3000";
app.listen(port, () => {
  console.log("Listening on port", port);
});
